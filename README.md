# Fabrizio Cozza

Job Vacancy Application
=======================

[![build status](https://gitlab.com/FabrizioCozza/job-vacancy/badges/master/pipeline.svg)](https://gitlab.com/FabrizioCozza/job-vacancy/commits/master) 

[![heroku link](https://cdn.iconscout.com/icon/free/png-128/heroku-225989.png)](https://volantisjobvacancystagingfabri.herokuapp.com/)
